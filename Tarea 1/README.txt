El programa realizado para esta tarea se encuentra en el lenguaje de programacion llamado Python
Realiza los calculos de las simulaciones de 4 tipos de predictores distintos de branches
Los predictores son:
-Predictor Bimodal
-Predictor con historia global
-Predictor con historia privada
-Predictor por torneo

Dependencias de programa:
-Terminal para ejecutar el programa
-Interprete de python a partir de la version 2.7
-Modulo de python "sys"

Instrucciones para ejecutarlo

El programa deber´a poder ejecutarse de la siguiente forma:

                        gunzip -c branch-trace-gcc.trace.gz | python predictor.py -s < # > -bp < # > -gh < # > -ph < # > -o < # >

Esto se realiza estando en la carpeta en la que se encuentra el archivo, de lo contrario debe indicar la ruta para llegar a este despues de ejecutar "python"

Los parametros o # estan dados de la siguiente forma

1. Tamano de la tabla BTH. (-s): El valor que desee el usuario
2. Tipo de prediccion (-bp): 0 es Bimodal, 1 es de historia global, 2 es con historia privada y 3 por torneo
3. Tamano del registro de prediccion global (-gh)
4. Tamano de los registros de historia privada (-ph)
5. Salida de la simulacion (-o): Si se elije un 1, se generara el archivo de salida de los primeros 5000 casos. Si no, igual es necesario escribir un numero para correrlo


Ejemplo:
En consola
                    gunzip -c branch-trace-gcc.trace.gz | python predictor.py -s 4 -bp 1 -gh 3 -ph 3 -o 1
                    
Esto genera los siguientes parametros:

        Branch prediction type: Con historia global
        BHT size (entries): 16
        Global history register size: 3
        Private history register size: 3
        Ademas de generar el archivo de salida del nombre Gshare.txt

