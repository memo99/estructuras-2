import sys

#Es la funcion principal que realiza el predictor bimodal, recibe el S y la opcion de si se quiere una tabla con los primeros
#Muestra todo en consola por medio de la funcion anterior por lo que no es necesario retornar nada
def bimodal(s,o):
    totalesT=0
    totalesN=0
    correctosT=0
    correctosN=0
    bht= crear_bht(s) #Se crea la BHT

    f = sys.stdin.read() #Para leer archivo
    lineas = f.split("\n") #Crea una lista con las lineas del archivo separadas por los enter
    largo=len(lineas)-1

    if o==1: #En caso de que se quiera escribir el documento de texto
        file = open("Bimodal.txt", 'w')
        file.write("PC\t\t"+"Outcome\t"+ "Prediction\t"+" correcto/incorrecto\t"+"\n")

    for x in range(largo):
        y = lineas[x].split(" ") #Separa el PC de la ejecucion del salto

        indice = indexar(y[0],s) #Utiliza los S bits menos significativos para indexar el BHT

        newtotalesN,newcorrectosN,newtotalesT,newcorrectosT, pred, precision = logicaBimodal(bht,indice,y[1]) #Con la logica de esta funcion se obtiene lo necesario para actualizar los contadores y las predicciones
        #Se actualizan los contadores de los totales y los correctos
        totalesT= totalesT+ newtotalesT
        totalesN= totalesN + newtotalesN
        correctosT= correctosT + newcorrectosT
        correctosN= correctosN + newcorrectosN

        if o==1 and x<=5000: #Se escribe lo solicitado en caso de que se haya elegido la opcion de escribir archivo
            file.write(str(y[0])+"\t"+str(y[1])+"\t"+ str(pred)+"\t\t" +str(precision)+"\n")

    mostrar(totalesN,totalesT,correctosN,correctosT) #Se muestra en pantalla

    return


#Funcion principal del gshare, en esta se llaman a otras  funciones
#Recibe el tamano del bht, el del gh y la indicacion de si se quiere archivo de txt
#No retorna nada pues todo lo muestra en consola
def gshare(s,gh,o):
    totalesT=0
    totalesN=0
    correctosT=0
    correctosN=0
    f = sys.stdin.read() #Lee el archivo
    lineas = f.split("\n")

    largo=len(lineas)-1

    if o==1: #Escribre el archivo en caso de que se pida
        file = open("Gshare.txt", 'w')
        file.write("PC\t\t"+"Outcome\t"+ "Prediction\t"+" correcto/incorrecto\t"+"\n")

    #Se crean las tablas necesarias
    bht= crear_bht(s)
    bgh= crear_bgh(gh)

    for x in range(largo):
        y = lineas[x].split(" ")

        indice = indexGshare(y[0],s,bgh)
        #Se llama a la funcion que realiza la logica para los predictores con historia y previamente se encontro el indice para acceder
        nuevoindice,bgh, newtotalesN,newcorrectosN,newtotalesT,newcorrectosT, pred, precision = logicaHistoria(bht,bgh,indice,y[1])
        bht[indice] = nuevoindice
        #Se actualizan contadores
        totalesT= totalesT+ newtotalesT
        totalesN= totalesN + newtotalesN
        correctosT= correctosT + newcorrectosT
        correctosN= correctosN + newcorrectosN

        if o==1 and x<=5000: #Los primeros 5000 se escriben en caso de que se pida
            file.write(str(y[0])+"\t"+str(y[1])+"\t"+ str(pred)+"\t\t" +str(precision)+"\n")

    mostrar(totalesN,totalesT,correctosN,correctosT)
    return


#Funcion principal para el predictor de historia privada
#Recibe s, ph dado en los parametros y la opcion de escribir o no el documento
#No retorna nada, pero muestra en consola los resultados
def pshare(s,ph,o):
    totalesT=0
    totalesN=0
    correctosT=0
    correctosN=0
    f = sys.stdin.read()
    lineas = f.split("\n")
    bht,pht= crear_phtybht(s,ph)


    largo=len(lineas)-1

    if o==1:
        file = open("Pshare.txt", 'w')
        file.write("PC\t\t"+"Outcome\t"+ "Prediction\t"+" correcto/incorrecto\t"+"\n")


    for x in range(largo):
        y = lineas[x].split(" ")
        indicePHT= indexar(y[0],s)
        phtactual=pht[indicePHT]
        indice = indexGshare(y[0],s,phtactual)
        #Se localizan los indices para acceder a la tabla por medio de las funciones creadas anteriormente
        nuevoindice,nuevoPHT, newtotalesN,newcorrectosN,newtotalesT,newcorrectosT, pred, precision =logicaHistoria(bht,pht[indicePHT],indice,y[1])
        #Actualizamos contadores
        totalesT= totalesT+ newtotalesT
        totalesN= totalesN + newtotalesN
        correctosT= correctosT + newcorrectosT
        correctosN= correctosN + newcorrectosN

        pht[indicePHT]=nuevoPHT
        bht[indice] = nuevoindice

        if o==1 and x<=5000: #En caso de querer escribir el archivo
            file.write(str(y[0])+"\t"+str(y[1])+"\t"+ str(pred)+"\t\t" +str(precision)+"\n")

    mostrar(totalesN,totalesT,correctosN,correctosT)
    return


#Funcion principal para el predictor por Torneo
#Recibe todos los parametros que pasa el usuario a la hora de correr el programa
#Muestra los resultados en consola, mas no retorna nada
def torneo(s,gh,ph,o):
    totalesT=0
    totalesN=0
    correctosT=0
    correctosN=0
    precision=0

    metapredictor = crear_bht(s)
    bhtp,pht= crear_phtybht(s,ph)
    bhtg= crear_bht(s)
    bgh= crear_bgh(gh)

    f = sys.stdin.read()
    lineas = f.split("\n")

    largo=len(lineas)-1
    #Se crean los contadores y las tablas de ambos predictores
    if o==1:
        file = open("Torneo.txt", 'w')
        file.write("PC\t\t"+"Outcome\t"+ "Prediction\t"+" correcto/incorrecto\t"+"\n")

    for x in range(largo):
        y = lineas[x].split(" ")
        #Se realiza el contador privado
        indicePHT= indexar(y[0],s)
        phtactual=pht[indicePHT]
        indicep = indexGshare(y[0],s,phtactual)
        nuevoindicep,nuevoPHT, newtotalesNp,newcorrectosNp,newtotalesTp,newcorrectosTp, predp, precisionp =logicaHistoria(bhtp,pht[indicePHT],indicep,y[1])
        pht[indicePHT]=nuevoPHT
        bhtp[indicep] = nuevoindicep
        #Se realiza el contador global
        indiceg = indexGshare(y[0],s,bgh)
        nuevoindiceg,bgh, newtotalesNg,newcorrectosNg,newtotalesTg,newcorrectosTg, predg, precisiong = logicaHistoria(bhtg,bgh,indiceg,y[1])
        bhtg[indiceg] = nuevoindiceg

        #Se elige el que dicte el metapredictor (Donde va del 1 al 4, desde strong prefer private hasta strong prefer global)
        #Caso que elije el predictor privado
        if metapredictor[indicePHT]==1 or metapredictor[indicePHT]==2 :
            prediccion=predp
            precision=precisionp
            totalesT=totalesT+newtotalesTp
            totalesN=totalesN+newtotalesNp
            correctosT=correctosT+newcorrectosTp
            correctosN=correctosN+newcorrectosNp
            contadorusado="P"

        #Caso que elije el predictor global
        if  metapredictor[indicePHT]==3 or metapredictor[indicePHT]==4:
            precision=precisiong
            prediccion=predg
            precision=precisiong
            totalesT=totalesT+newtotalesTg
            totalesN=totalesN+newtotalesNg
            correctosT=correctosT+newcorrectosTg
            correctosN=correctosN+newcorrectosNg
            contadorusado="G"

        #Se actualiza el metapredictor
        if precisiong=="Correcto" and precisionp=="Incorrecto" and metapredictor[indicePHT]<4:
            metapredictor[indicePHT]=metapredictor[indicePHT]+1
        if precisiong=="Incorrecto" and precisionp=="Correcto" and metapredictor[indicePHT]>1:
            metapredictor[indicePHT]=metapredictor[indicePHT]-1


        if o==1 and x<=5000:
            file.write(str(y[0])+"\t"+str(y[1])+"\t"+ str(prediccion)+"\t\t" +str(precision)+"\n")

    mostrar(totalesN,totalesT,correctosN,correctosT)

    return


#Funcion que se utiliza para que una vez que tengamos el indice del BHT al que queremos acceder
#Accedamos al respectivo 2bit counter y lo actualicemos.
#Recibe la BHT, el indice y el valor T o N del saltos
#Retorna si se realizo un salto (totalT o totalN), si la prediccion fue correcta (correctosT y correctosN), su prediccion y si fue correcto o incorrecto en palabras
def logicaBimodal(bht,indice,y1):
    totalesT=0
    totalesN=0
    correctosT=0
    correctosN=0
    prediccion= "T"
    precision="Incorrecto"
    if y1== "N": #Condicion establecida para el caso en que se haya ejecutado un Not taken
        totalesN=1
        if bht[indice]<=2: #Se predijo correctamente con un N
            prediccion="N"
            correctosN=1
            nuevoindice=1 #Corresponde al contador de 2 bits (1=N, 2=n, 3=t, 4=T)
            precision= "Correcto"

        else: #Caso en que se predijo de forma incorrecta con un T
            correctos=0
            nuevoindice=bht[indice]-1 #Contador de 2 bits que

    else: #Caso en que se ejecuta un Taken
        totalesT=1
        if bht[indice]<=2: #Se prefijo un N entonces falla la prediccion
            prediccion="N"
            nuevoindice=bht[indice]+1

        else: #Se predice el T correctamente
            precision= "Correcto"
            correctosT=1
            nuevoindice=4

    bht[indice] = nuevoindice #Se actualiza el 2bit counter

    return totalesN,correctosN,totalesT,correctosT, prediccion, precision

#Funcion utiliada para los predictores de historia que al igual que la anterior, realiza la actualizacion de contadores y da los datos sobre las predicciones realiadas por el predictor de saltos
#recibe el bht, el bgh o el pht segun corresponda, el indice y el T o N
#Retorna el array donde contiene la historia (privada o global), el nuevo contador, y todos los contadores y predicciones actualizadas
def logicaHistoria(bht,hist,indice,y1):
    totalesT=0
    totalesN=0
    correctosT=0
    correctosN=0
    prediccion= "T"
    precision="Incorrecto"
    if y1=="N": #Caso en que se ejecuta un N
        totalesN=1
        if bht[indice]<=2: #Se predice correctamente. El bht[indice] es nuestro 2bit counter con valores de 1 a 4
            prediccion="N"
            precision="Correcto"
            correctosN=1
            nuevoindice=1
            hist=hist[1:]
            hist= hist + "0"

        else: #Se predice un taken cuando era un N
            nuevoindice=bht[indice]-1
            hist=hist[1:]
            hist= hist + "0"

    else: #Se ejecuta un taken
        totalesT=1
        if bht[indice]<=2: #Se predice de forma incorrecta
            prediccion="N"
            nuevoindice=bht[indice]+1
            hist=hist[1:]
            hist= hist +"1"

        else: #Bien predicho el salto
            precision="Correcto"
            correctosT=1
            nuevoindice=4
            hist=hist[1:]
            hist= hist +"1"

    return nuevoindice, hist, totalesN,correctosN,totalesT,correctosT, prediccion, precision #Se retorna el nuevo 2bit counter, y los valores totales y correctos de los saltos tomados asi como si estaban correctos o no


#Muestra en pantalla los resultados que se piden en el enunciado.
#Recibe los totales de N, T, N correctos y T correctos para realizar los calculos del numero de branches, de incorrectas y de porcentaje
#No retorna nada debdio a que imprime todo en pantalla
def mostrar(totalesN,totalesT,correctosN,correctosT):
    branches= totalesN+totalesT
    incorrectosT= totalesT-correctosT
    incorrectosN= totalesN - correctosN
    porcentaje= 100*(correctosN+correctosT)/(branches)


    print("Number of branch: "+ str(branches))
    print("Number of correct prediction of taken branches: "+str(correctosT))
    print("Number of incorrect prediction of taken branches: "+str(incorrectosT))
    print("Correct prediction of not taken branches: "+str(correctosN))
    print("incorrect prediction of not taken branches: "+str(incorrectosN))
    print("Percentage of correct predictions: " + str(porcentaje))
    return



#Funcion para crear la tabla BHT y agregarle los valores iniciales del 2 bit counter que seria strong not taken dado por el numero 1
#Recibe S y crea una lista de tamano 2 a la s
#Retorna la lista
def crear_bht(s):
    bht= []
    tamano=pow(2, s)
    for x in range(tamano):
        bht.append(1)

    return bht


#Recibe el numero X como un string de ints y el tamano s
#Con esto lo pasa a binario para cortar la cantidad N de bits necesarios y luego lo devuelve a decimal
#Retorna el  indice en binario
def indexar(x,s):
    x= int(x)
    completobin =  bin(x)
    tam= len(completobin)-s
    completobin = completobin[tam:]

    return int(completobin,2)


#Este da el indice para acceder a los contadores en la  de historia global
# Recibe el numero como un string, el tamano s y la tabla de historia global
#Retorna el indice en decimal
def indexGshare(x,s,gbh):
    x= int(x)
    xbin =  bin(x)
    xbin = xbin[2:] #Se le quitan los dos primeros que no son numeros
    y = int(xbin,2) ^ int(gbh,2) #Se realiza un or entre el pc y el gbh
    completobin = bin(y)
    tam= len(completobin)-s #Se pasa a binario y se corta para que quede con N bits
    completobin = completobin[tam:]


    return int(completobin,2) #Lo retornamos como decimal


#Esta funcion sirve para crear la bgh en el Gshare
#Recibe el valor gh
#Retorna la lista ya hecha con valores de 0 en ella que indican que no se ha dado T
def crear_bgh(gh):
    bgh= ""
    for x in range(gh):
        bgh = bgh +"0"
    return bgh


#Funcion que crea la PHT y la BHT para el predictor de historia privada
#Recibe los tamanos dados por s y ph
#Retorna ambas tablas echas con contadores que inician en N o 1
def crear_phtybht(s,ph):
    pht_entrada= crear_bgh(ph)
    pht=[]
    bht= []
    tamano=pow(2, s)
    for x in range(tamano):
        bht.append(1)
        pht.append(pht_entrada)
    return bht,pht


#Funcion que imprime los parametros seleccionados por le usuario
#Toma los parametros dados por el usuario a la hora de correrlo
#Muestra en pantalla, pero no retorna nada
def param(s,gh,ph,eleccion):
    size= pow(2,s)
    print("Parametros dados \n")
    print("Branch prediction type: " + eleccion + "\n")
    print("BHT size (entries): " + str(size) + "\n")
    print("Global history register size: " + str(gh) + "\n")
    print("Private history register size: " + str(ph) + "\n")
    print("\nResultados de las simulaciones \n")
    return


def main(): #Main del programa donde se toman los datos el usuario por medio de la consola y se imprimen los resultados
    s=int(sys.argv[2])
    bp=int(sys.argv[4])
    gh=int(sys.argv[6])
    ph=int(sys.argv[8])
    o=int(sys.argv[10])
    eleccion=""

    if bp==0:#Caso Bimodal
        eleccion="Bimodal"
        param(s,gh,ph,eleccion)
        bimodal(s,o)


    if bp==1: #Caso con historia global
        eleccion="Con historia global"
        param(s,gh,ph,eleccion)
        gshare(s,gh,o)

    if bp==2: #Caso con historia privada
        eleccion="Con historia privada"
        param(s,gh,ph,eleccion)
        pshare(s,ph,o)

    if bp==3: #Caso por torneo
        eleccion="Por torneo"
        param(s,gh,ph,eleccion)
        torneo(s,gh,ph,o)




if __name__ == '__main__':
    main()
